#include "stdafx.h"
void FileParser::parseInputFile(std::string filePath, TravelerInstance &instance) {
	
	std::ifstream inputStream(filePath, std::ifstream::in);

	if (!inputStream.is_open()) {
		throw L"Plik nie istnieje!!!";
	}

	if (inputStream.bad()) {
		throw L"B��d odczytu pliku!!!";
	}

	std::string buffer;
	std::string param;
	int params = 0;
	//P�tla szukaj�ca parametr�w
	while (params != FileParser::PARAMS_AMOUNT) {
		std::getline(inputStream, buffer);
		if (Utils::String::isLineComment(buffer) || Utils::String::isEmpty(buffer)) {
			continue;
		}
		if (Utils::String::startsWith(buffer, FileParser::SIZE_PARAM)) {
			param = buffer.substr(SIZE_PARAM.size(), buffer.size() - 1);
			instance.initializeIntCitiesTab(std::stoi(param));
			++params;
		}
		else if (Utils::String::startsWith(buffer, FileParser::NAMES_IN_PARAM)) {
			param = buffer.substr(NAMES_IN_PARAM.size(), buffer.size() - 1);
			param == FileParser::TRUE_STRING ? instance.citiesNameList = new std::list<std::string>() : instance.citiesNameList = nullptr;
			++params;
		}
	}

	//Dopiero po znalezieniu wszystkich leci dalej
	int rows = 0;
	int startindex = 0;
	int endIndex = 0;
	bool namesAvailable = instance.areNamesAvailable();

	std::vector<std::string> splitBuffer;
	while (rows != instance.intCitiesTab.size()) {
		std::getline(inputStream, buffer);
		if (Utils::String::isLineComment(buffer) || Utils::String::isEmpty(buffer)) {
			continue;
		}

		Utils::String::split(buffer, FileParser::SPLIT_SEPARATOR, splitBuffer);

		startindex = 0;
		endIndex = instance.intCitiesTab.size();
		if (namesAvailable) {
			instance.citiesNameList->push_back(splitBuffer[0]);
			startindex++;
			endIndex++;
		} 

		for (int i = startindex; i < endIndex; i++) {
			instance.intCitiesTab[rows][namesAvailable == true ? i - 1 : i] = std::stoi(splitBuffer[i]);
		}
		
		++rows;
	}

	//Nadpisanie g��wnej przek�tnej macie�y 0'ami
	for (int i = 0; i < instance.intCitiesTab.size(); i++) {
		instance.intCitiesTab[i][i] = 0;
	}
}

const int FileParser::PARAMS_AMOUNT = 2;
const std::string FileParser::TRUE_STRING = "true";
const std::string FileParser::SIZE_PARAM = "size=";
const std::string FileParser::NAMES_IN_PARAM = "names=";
const std::string FileParser::SPLIT_SEPARATOR = " ";