// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include <list>
#include <string>
#include <ios>
#include <fstream> 
#include <iostream>
#include <vector>
#include <random>
#include <conio.h>
#include <locale>
#include <boost/algorithm/string.hpp>
#include <windows.h>
#include <time.h>
#include <set>
#include <unordered_set>
#include <tuple>
#include <map>


typedef std::pair<int, int> Edge;
#include "BnBMatrix.h"
#include "Result.h"
#include "targetver.h"
#include "InputManager.h"
#include "TravelerInstance.h"
#include "TabuSearch.h"
#include "Utils.h"
#include "FileParser.h"
#include "InterfaceHandler.h"
#include "DynamicSolver.h"
#include "Genetic.h"
#include "2approx.h"
#include "Algorithms.h"




// TODO: reference additional headers your program requires here
