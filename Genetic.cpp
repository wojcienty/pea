#include "stdafx.h"

bool Genetic::Comparator::operator() (std::vector<int>& first, std::vector<int>& second) {
	int firstLenght = Algorithms::Utils::computeWayLenght(first, this->genetic.travelerInstance);
	int secondLenght = Algorithms::Utils::computeWayLenght(second, this->genetic.travelerInstance);
	return firstLenght < secondLenght;
}

Genetic::Genetic(TravelerInstance travelerInstance, int sizeOfPopulation, int amountOfGenerations) {
	this->travelerInstance = travelerInstance;
	this->sizeOfPopulation = sizeOfPopulation;
	this->amountOfGenerations = amountOfGenerations;

	this->engine = std::mt19937(rd());
	this->mutationDistibution = std::uniform_int_distribution<int>(0, sizeOfPopulation - 1);
	this->swapDistibution = std::uniform_int_distribution<int>(1, this->travelerInstance.intCitiesTab.size() - 1);
	population = std::vector<std::vector<int>>(sizeOfPopulation);
	for (int i = 0; i < sizeOfPopulation; i++) {
		population[i] = Algorithms::Utils::generateRandomSolution(travelerInstance.startCity, travelerInstance.intCitiesTab.size());
	}

	this->selected = std::vector<std::vector<int>>((int) (0.1 * sizeOfPopulation));
	bestSolutionSoFar = Algorithms::Utils::generateRandomSolution(travelerInstance.startCity, travelerInstance.intCitiesTab.size());
}

//We� 10 i zr�b na nich 2 losowe swapy
void Genetic::mutation() {
	int first, second, index;
	for (int i = 0; i < 10; i++) {
		first = -1;
		second = -1;

		index = this->mutationDistibution(engine);
		first = this->swapDistibution(engine);
		do {
			second = this->swapDistibution(engine);
		} while (first == second);

		::Utils::Vector::swap(this->population[index], first, second);
	}
}

//Sortowanie i bierz od lewej wed�ug przesuni�tego normalnego //p�ki co po kolei 
void Genetic::selection() {
	int size = selected.size();
	selected.clear();
	this->selected = std::vector<std::vector<int>>(size);
	std::sort(population.begin(), population.end(), Comparator(*this));

	for (int i = 0; i < size; i++) {
		this->selected[i] = population[i];
	}
	this->bestSolutionSoFar = population[0];
}

//PMX
void Genetic::crossing() {

	int k1 = -1;
	int k2 = -1;
	int subtraction = -1;
	do {
		k1 = swapDistibution(engine);
		k2 = swapDistibution(engine);
		subtraction = k2 - k1;
	} while (k1 == k2 || subtraction <= 2);

	std::vector<int> mother, father, dauther, son;
	for (int i = 0; i < selected.size() - 1; i += 2) {
		mother = selected[i];
		father = selected[i + 1];

		dauther = std::vector<int>(mother.size());
		dauther[0] = travelerInstance.startCity;
		dauther[dauther.size() - 1] = travelerInstance.startCity;

		son = std::vector<int>(father.size());
		son[0] = travelerInstance.startCity;
		son[son.size() - 1] = travelerInstance.startCity;

		std::map<int, int> pierwsze_odwzorowanie;
		std::map<int, int> odwzorowanie_odwrotne;

		//Kopiowanie
		for (int i = 1; i < dauther.size() - 1; i++) {
			if (i > k1 && i < k2) {
				dauther[i] = mother[i];
				son[i] = father[i];
			} else {
				dauther[i] = -1;
				son[i] = -1;
			}
			pierwsze_odwzorowanie.insert({father[i], mother[i]});
			odwzorowanie_odwrotne.insert({mother[i], father[i]});
		}

		//Doklejanie
		for (int i = 1; i < father.size() - 1; i++) {
			if (dauther[i] == -1) {
				if (!Algorithms::Utils::contains(dauther, 1, dauther.size() - 1, father[i])) {
					dauther[i] = father[i];
				}
			}
			if (son[i] == -1) {
				if (!Algorithms::Utils::contains(son, 1, son.size() - 1, mother[i])) {
					son[i] = mother[i];
				}
			}
		}
		//Uzupe�nianie
		for (int i = 1; i < father.size() - 1; i++) {
			if (dauther[i] == -1) {
				try {
					int value = pierwsze_odwzorowanie.at(father[i]);
					if (!Algorithms::Utils::contains(dauther, 1, dauther.size() - 1, value)) {
						dauther[i] = value;
					}
				} catch (const std::exception&) {
					//nie ma to trudno
				}
			}
			if (son[i] == -1) {
				try {
					int value = odwzorowanie_odwrotne.at(mother[i]);
					if (!Algorithms::Utils::contains(son, 1, son.size() - 1, value)) {
						son[i] = value;
					}
				} catch (const std::exception&) {
					//nie ma to trudno
				}
			}
		}
		//Braki
		for (int i = 1; i < father.size() - 1; i++) {
			if (dauther[i] == -1) {
				int value = pierwsze_odwzorowanie.at(father[i]);
				value = pierwsze_odwzorowanie.at(value);
				while (true) {
					if (!Algorithms::Utils::contains(dauther, 1, dauther.size() - 1, value)) {
						dauther[i] = value;
						break;
					}
					value = pierwsze_odwzorowanie.at(value);
				}
			}

			if (son[i] == -1) {
				int value = odwzorowanie_odwrotne.at(mother[i]);
				value = odwzorowanie_odwrotne.at(value);
				while (true) {
					if (!Algorithms::Utils::contains(son, 1, son.size() - 1, value)) {
						son[i] = value;
						break;
					}
					value = odwzorowanie_odwrotne.at(value);
				}
			}
		}


		population[population.size() - (i + 1)] = dauther;
		population[population.size() - (i + 2)] = son;
	}
}


