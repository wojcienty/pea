#include "stdafx.h"



std::pair<int, int> getMaxPenatlyIndex(std::vector<std::vector<int>> matrix, std::vector<int> &minimums) {
	int maxPenatly = INT_MIN;
	std::pair<int, int> output;
	for (int row = 0; row < matrix.size(); row++) {
		for (int col = 0; col < matrix.size(); col++) {
			if (matrix[row][col] != BnBMatrix::INF && matrix[row][col] == 0) {
				int maxVal = minimums[row] + minimums[matrix.size() + col];
				if (maxVal > maxPenatly) {
					maxPenatly = maxVal;
					output = std::pair<int, int>(row, col);
				}
			}
		}
	}

	return output;
}

std::pair<std::vector<Edge>, int> Algorithms::branch(BnBMag mag, CheckList &toCheck, int bestUpper) {

	int bound = mag.upperBound;
	while (mag.matrix.matrix.size() != 2) {
		int instanceSize = mag.matrix.matrix.size();
		for (int i = 0; i < instanceSize; ++i) {
			bound += mag.matrix.getRowMinimum(i).first;
		}
		for (int i = 0; i < instanceSize; ++i) {
			bound += mag.matrix.getColumnMinimum(i).first;
		}

		if (bound > bestUpper) {
			return std::pair<std::vector<Edge>, int>(mag.edges, -1);
		}

		//Znalezienie minim�w z kolumn i wierszy po pierwszej subtrakcji
		std::vector<int> minimums;
		for (int i = 0; i < instanceSize; ++i) {
			minimums.push_back(mag.matrix.getRowMinimumWithZeros(i));
		}
		for (int i = 0; i < instanceSize; ++i) {
			minimums.push_back(mag.matrix.getColMinimumWithZeros(i));
		}

		std::pair<int, int> coord = getMaxPenatlyIndex(mag.matrix.matrix, minimums);

		BnBMatrix checkMatrix = mag.matrix;
		checkMatrix.matrix[coord.first][coord.second] = BnBMatrix::INF;
		toCheck.push_back(BnBMag(checkMatrix, bound + mag.matrix.matrix[coord.first][coord.second], mag.edges));

		Edge e = Edge(mag.matrix.rowsIndexes[coord.first], mag.matrix.columsIndexes[coord.second]);
		mag.edges.push_back(e);
		mag.matrix.deleteRowAndCol(coord.first, coord.second, mag.edges);
	}

	for (int i = 0; i < 2; ++i) {
		bound += mag.matrix.getRowMinimum(i).first;
	}
	for (int i = 0; i < 2; ++i) {
		bound += mag.matrix.getColumnMinimum(i).first;
	}

	if (bound > bestUpper) {
		return std::pair<std::vector<Edge>, int>(mag.edges, -1);
	}

	if (mag.matrix.matrix[0][0] == BnBMatrix::INF || mag.matrix.matrix[1][1] == BnBMatrix::INF) {
		mag.edges.push_back(Edge(mag.matrix.rowsIndexes[0], mag.matrix.columsIndexes[1]));
		mag.edges.push_back(Edge(mag.matrix.rowsIndexes[1], mag.matrix.columsIndexes[0]));
	} else {
		mag.edges.push_back(Edge(mag.matrix.rowsIndexes[0], mag.matrix.columsIndexes[0]));
		mag.edges.push_back(Edge(mag.matrix.rowsIndexes[1], mag.matrix.columsIndexes[1]));
	}

	return std::pair<std::vector<Edge>, int>(mag.edges, bound);
}

Result Algorithms::annealing(TravelerInstance instance) {
	
	double endTemperature = 0.0000001;
	double geometricCoolingParameter = 0.99;
	double initialProbability = 0.9;

	std::vector<std::vector<int>> cities = instance.intCitiesTab;
	std::vector<int> way = std::vector<int>(cities.size() + 1);
	way[0] = instance.startCity;
	way[way.size() - 1] = instance.startCity;

	int tempIndex = 1;
	for (int i = 1; i < cities.size(); i++) {
		if (i != instance.startCity) {
			way[tempIndex] = i;
			tempIndex++;
		} 
	}

	bool endOfAnnealing = false;
	int bestLengthSoFar = Utils::computeWayLenght(way, instance);

	int swapIndex1, swapIndex2, actualLength;
	double probabilityOfAcceptance;
	bool acceptPermutation;

	int maxLength = INT_MIN, minLength = INT_MAX;
	for (int row = 0; row < cities.size(); row++) {
		for (int col = 0; col < cities.size(); col++) {
			if (row == col) {
				continue;
			} else {
				if (cities[row][col] > maxLength) {
					maxLength = cities[row][col];
				} else if (cities[row][col] < minLength) {
					minLength = cities[row][col];
				}
			}
		}
	}

	double startTemp = -(maxLength - minLength) / log(initialProbability);
	double deltaTemp = 0;

	std::random_device rd;
	std::mt19937 engine(rd());
	std::uniform_int_distribution<int> intDistribution(1, way.size() - 2);
	std::uniform_real_distribution<double> doubleDistribution(0.0, 1.0);

	clock_t start = clock();
	for (double temp = startTemp; temp > endTemperature; deltaTemp = temp, temp *= geometricCoolingParameter) {
		deltaTemp = deltaTemp - temp;
		acceptPermutation = false;

		//Losowanie index�w do zamiany
		swapIndex1 = intDistribution(engine);
		swapIndex2 = intDistribution(engine);

		//Zamiana
		::Utils::Vector::swap(way, swapIndex1, swapIndex2);

		actualLength = Utils::computeWayLenght(way, instance);
		//Decyzja o akceptacji permutacji
		if (actualLength < bestLengthSoFar) {
			bestLengthSoFar = actualLength;
			acceptPermutation = true;
		} else if (actualLength > bestLengthSoFar) {
			probabilityOfAcceptance = exp((double)(actualLength - bestLengthSoFar) / temp);
			//probabilityOfAcceptance = exp(-deltaTemp/temp);
			if(probabilityOfAcceptance < doubleDistribution(engine)) acceptPermutation = true;
		}

		if (!acceptPermutation) {
			::Utils::Vector::swap(way, swapIndex2, swapIndex1);
		}
	}
	clock_t end = clock();

	return Result(way, end - start, L"Algorytm symulowanego wy�arzania.");
}

Result Algorithms::tabuSearch(TravelerInstance instance, int numberOfSteps) {
	using Algorithms::Utils::computeWayLenght;
	using ::Utils::Vector::swap;
	

	TabuSearch tabuSearch(instance);
	std::vector<int> bestWay = tabuSearch.generateRandomSolution();
	
	clock_t CLOCK_START = 0, CLOCK_END = 0;
	CLOCK_START = clock();

	std::vector<int> currentSolution;
	std::pair<int, int> actualSwap;
	int bestWayLenght = computeWayLenght(bestWay, instance);
	int actualWayLength = 0;
	bool acceptMove = false;

	for (int i = 0; i < numberOfSteps; i++) {
		acceptMove = false;
		//we� jakie� nowe rozwi�zanie
		actualSwap = tabuSearch.makeRandomSwap(bestWay);
		//sprawd� czy ruch jest w tabuli�cie
		bool isInTabuList = tabuSearch.isSwapInTabuList(actualSwap);
		actualWayLength = computeWayLenght(bestWay, instance);
		//Je�eli jest to sprawd� czy jest lepsze
		if (isInTabuList) {
			if (actualWayLength < bestWayLenght) {
				//Je�eli jest to ruch do zamie� tak czy siak
				bestWayLenght = actualWayLength;
				acceptMove = true;
				tabuSearch.noBetterSolutionLoops = 0;
			} else {
				//Je�eli nie to increment s�abych rozwi�za� i pr�ba ucieczki
				tabuSearch.noBetterSolutionLoops++;
				tabuSearch.diversification(bestWay, &bestWayLenght);
			}
		} else {
			//Je�eli nie ma to sprawd� czy lepsze
			if (actualWayLength < bestWayLenght) {
				//Je�eli jest to zamie�
				//Dodaj to tabuListy
				bestWayLenght = actualWayLength;
				acceptMove = true;
				tabuSearch.noBetterSolutionLoops = 0;
				tabuSearch.addToTabuList(actualSwap);
			} else {
				//Je�eli nie increment s�abych rozwi�za� i pr�ba ucieczki
				tabuSearch.noBetterSolutionLoops++;
				tabuSearch.diversification(bestWay, &bestWayLenght);
			}
		}

		if (!acceptMove) {
			swap(bestWay, actualSwap.second, actualSwap.first);
		}
	}

	CLOCK_END = clock();
	return Result(bestWay, CLOCK_END - CLOCK_START, L"Algorytm lokalnego przeszukiwania TabuSearch.");
}

Result Algorithms::branchAndBound(TravelerInstance instance) {
	std::vector<Edge> edges;
	BnBMatrix matrix(instance.intCitiesTab);
	CheckList toCheck;
	clock_t start, end;
	int lowBound = 0;

	start = clock();
#pragma region lowbound
	for (int i = 0; i < matrix.size(); ++i) {
		lowBound += matrix.getRowMinimum(i).first;
	}
	for (int i = 0; i < matrix.size(); ++i) {
		lowBound += matrix.getColumnMinimum(i).first;
	}
#pragma endregion
	
	BnBMag mag(matrix, lowBound, edges);
	std::pair<std::vector<Edge>, int> firstSolution = branch(mag, toCheck, INT_MAX);
	int upperBound = firstSolution.second;
	std::vector<Edge> bestSolution = firstSolution.first;
	while (toCheck.size() != 0) {
		BnBMag mag = *toCheck.begin();
		toCheck.erase(toCheck.begin());
		if (mag.upperBound >= upperBound) {
			continue;
		} else {
			std::pair<std::vector<Edge>, int> temp = branch(mag, toCheck, upperBound);
			if (temp.second != -1) {
				bestSolution = temp.first;
				upperBound = temp.second;
			}
		}
	}

	end = clock();
#pragma region wayShowComputing
	std::vector<int> way;
	int cityToFind = instance.startCity;
	way.push_back(cityToFind);
	do {
		for (int i = 0; i < bestSolution.size(); i++) {
			if (bestSolution[i].first == cityToFind) {
				way.push_back(bestSolution[i].second);
				cityToFind = bestSolution[i].second;
				break;
			}
		}
	} while (cityToFind != instance.startCity);
#pragma endregion

	return Result(way, end - start, L"Algorytm podzia��w i ogranicze�.");
}

Result Algorithms::bruteForce(TravelerInstance instance) {
	std::vector<std::vector<int>> cities = instance.intCitiesTab;
	std::vector<int> way = std::vector<int>(cities.size() + 1);
	way[0] = instance.startCity;
	way[way.size() - 1] = instance.startCity;

	int tempIndex = 1;
	for (int i = 1; i < cities.size(); i++) {
		if (i != instance.startCity) {
			way[tempIndex] = i;
			tempIndex++;
		}
	}

	clock_t before, after;
	std::vector<int> bestWay;
	int bestLength = INT_MAX;
	int actualLength = 0;
	before = clock();
	int factorial = Algorithms::Utils::factorial(way.size() - 2);
	for (int i = 0; i < factorial; i++) {
		std::next_permutation(way.begin() + 1, way.end() - 1);
		actualLength = Algorithms::Utils::computeWayLenght(way, instance);
		if (bestLength > actualLength) {
			bestLength = actualLength;
			bestWay = way;
		}
	}
	after = clock();

	return Result(bestWay, after - before, L"Przegl�d zupe�ny.");
}

Result Algorithms::dynamic(TravelerInstance instance) {

	clock_t start, end;
	DynamicSolver solver = DynamicSolver(instance);

	start = clock();
	solver.nonRecursionSolve();
	/*solver.solve();*/
	end = clock();
	return Result({1, 2}, end - start, L"Algorytm programowania dynamicznego.");
}

Result Algorithms::genetic(TravelerInstance instance, int populationSize, int generationsAmount) {
	clock_t start, end;
	Genetic * geneticSolver = new Genetic(instance, populationSize, generationsAmount);

	start = clock();

	for (int i = 0; i < geneticSolver->amountOfGenerations; i++) {
		geneticSolver->selection();
		geneticSolver->crossing();
		geneticSolver->mutation();
	}

	geneticSolver->selection();
	end = clock();
	return Result(geneticSolver->bestSolutionSoFar, end - start, L"Algorytm genetyczny.");
}

Result Algorithms::approx(TravelerInstance instance) {
	clock_t start, end;
	Approx2Solver approx2Solver = Approx2Solver(instance);

	approx2Solver.prepareRepresentation();
	start = clock();
	approx2Solver.mst();
	approx2Solver.eulerCycle();
	approx2Solver.makeWay();
	end = clock();
	return Result(approx2Solver.way, end - start, L"Algorytm 2 aproksymacyjny.");
}
