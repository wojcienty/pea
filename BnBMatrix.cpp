#include "stdafx.h"


const u_char BnBMatrix::COLUMN = 0;
const u_char BnBMatrix::ROW = 1;
const char BnBMatrix::INF = -1;

BnBMatrix::BnBMatrix(std::vector<std::vector<int>> matrix) {
	this->matrix = matrix;
	this->VU = std::vector<int>(matrix.size());
	this->UeV = std::vector<int>(matrix.size());
	for (int i = 0; i < matrix.size(); i++) {
		this->columsIndexes.push_back(i);
		this->rowsIndexes.push_back(i);
		this->matrix[i][i] = BnBMatrix::INF;
		this->VU[i] = i;
		this->UeV[i] = i;
	}
}

std::pair<int, u_int> BnBMatrix::getRowMinimum(int row) {
	int min = INT_MAX;
	u_int index = 0;
	for (int i = 0; i < matrix.size(); i++) {
		if (matrix[row][i] == BnBMatrix::INF) continue;
		if (matrix[row][i] < min) {
			min = matrix[row][i];
			index = i;
		}
	}
	for (int i = 0; i < matrix.size(); i++) {
		if (matrix[row][i] == BnBMatrix::INF) continue;
		matrix[row][i] -= min;
	}
	return std::pair<int, u_int>(min, index);
}

std::pair<int, u_int> BnBMatrix::getColumnMinimum(int column) {
	int min = INT_MAX;
	int index = 0;
	for (int i = 0; i < matrix.size(); i++) {
		if (matrix[i][column] == BnBMatrix::INF) continue;
		if (matrix[i][column] < min) {
			min = matrix[i][column];
			index = i;
		}
	}
	for (int i = 0; i < matrix.size(); i++) {
		if (matrix[i][column] == BnBMatrix::INF) continue;
		matrix[i][column] -= min;
	}
	return std::pair<int, u_int>(min, index);
}

int BnBMatrix::getRowMinimumWithZeros(int rowIndex) {

	std::vector<int> row;
	for (int i = 0; i < matrix.size(); i++) {
		if (matrix[rowIndex][i] != BnBMatrix::INF) {
			row.push_back(matrix[rowIndex][i]);
		}
	}
	std::sort(row.begin(), row.end());
	return row.size() > 1 ? row[1] : row[0];
}

int BnBMatrix::getColMinimumWithZeros(int columnIndex) {
	std::vector<int> column;
	for (int i = 0; i < matrix.size(); i++) {
		if (matrix[i][columnIndex] != BnBMatrix::INF) {
			column.push_back(matrix[i][columnIndex]);
		}
	}
	std::sort(column.begin(), column.end());

	return column.size() > 1 ? column[1] : column[0];
}

bool BnBMatrix::isInEdgesList(int value, std::vector<Edge> &edges) {
	for (int i = 0; i < edges.size(); i++) {
		if (edges[i].first == value) {
			return true;
		}
	}
	return false;
}

int BnBMatrix::findZeroIndex(u_char colOrRow, int index, std::vector<Edge> &edges) {
	if (colOrRow == BnBMatrix::ROW) {
		for (int i = 0; i < matrix.size(); i++) {
			if (matrix[index][i] == 0) {
				if (isInEdgesList(rowsIndexes[i], edges)) {
					continue;
				} else {
					return i;
				}
			}
		}
	} else {
		for (int i = 0; i < matrix.size(); i++) {
			if (matrix[i][index] == 0) {
				if (isInEdgesList(columsIndexes[i], edges)) {
					continue;
				} else {
					return i;
				}
			}
		}
	}
}

int BnBMatrix::size() {
	return matrix.size();
}

std::pair<u_char, int> BnBMatrix::getRowOrColToDelete(std::vector<int> &minimums) {

	//Odnalezienie maximum z minim�w
	int maxValue = -1;
	int maxIndex;
	for (int i = 0; i < minimums.size(); i++) {
		if (minimums[i] > maxValue) {
			maxValue = minimums[i];
			maxIndex = i;
		}
	}

	//Wybieramy model skracania, wiersz/kolumna, czy kolumna/wiersz
	//Dzielenie zadzia�a, poniewa� macierz jest symetryczna nawet po skr�ceniu
	bool columnHasMaximum = false;
	if (maxIndex >= minimums.size() / 2) {
		maxIndex %= (minimums.size() / 2); //Kolumna
		columnHasMaximum = true;
	}

	return std::pair<u_char, int>(columnHasMaximum ? BnBMatrix::COLUMN : BnBMatrix::ROW, maxIndex);
}


bool BnBMatrix::blockReturn(int row, int col) {
	//Blokowanie przej�cia
	int columnBlockIndex = -1;
	int rowBlockIndex = -1;
	//Szukamu nr wiersza w kolumnach
	for (int i = 0; i < rowsIndexes.size(); i++) {
		if (rowsIndexes[row] == columsIndexes[i]) {
			columnBlockIndex = i;
		}
		if (columsIndexes[col] == rowsIndexes[i]) {
			rowBlockIndex = i;
		}
	}

	if (columnBlockIndex != -1 && rowBlockIndex != -1) {
		matrix[rowBlockIndex][columnBlockIndex] = BnBMatrix::INF;
		return true;
	}

	return false;
}

bool BnBMatrix::blockReturnOut(int row, int col) {
	int rowIndex = -1, colIndex = -1;
	for (int i = 0; i < rowsIndexes.size(); i++) {
		if (rowsIndexes[i] == row) {
			rowIndex = i;
		}
	}
	for (int i = 0; i < columsIndexes.size(); i++) {
		if (columsIndexes[i] == col) {
			colIndex = i;
		}
	}
	if (rowIndex != -1 && colIndex != -1) {
		matrix[rowIndex][colIndex] = BnBMatrix::INF;
		return true;
	}

	return false;
}

std::pair<int, int> blockCycles(Edge &last, std::vector<int> &VU, std::vector<int> &UeV) {
	int unionPrev = VU[last.second];
	int unionNext = VU[last.first];
	VU[last.second] = VU[last.first];

	for (int i = 0; i < VU.size(); i++) {
		if (VU[i] == unionPrev) {
			VU[i] = unionNext;
		}
	}

	UeV[unionNext] = UeV[unionPrev];

	return std::pair<int, int>(UeV[unionNext], unionNext);
}

void BnBMatrix::deleteRowAndCol(u_int row, u_int col, std::vector<Edge> &edges) {
	int instanceSize = matrix.size();
	std::vector<int>::iterator columnsIter;
	std::vector<std::vector<int>>::iterator rowIter;

	int colIndex = -1;
	int rowIndex = -1;
	//blokada powrotu
	blockReturn(row, col);
	//blokada cykli
	std::pair<int, int> coords = blockCycles(edges.back(), VU, UeV);
	blockReturnOut(coords.first, coords.second);
	
	for (int i = 0; i < instanceSize; i++) {
		columnsIter = matrix[i].begin();
		std::advance(columnsIter, col);
		matrix[i].erase(columnsIter);
	}

	rowIter = matrix.begin();
	std::advance(rowIter, row);
	matrix.erase(rowIter);

	columnsIter = columsIndexes.begin();
	std::advance(columnsIter, col);
	columsIndexes.erase(columnsIter);

	std::vector<int>::iterator rowsIndexesIter;
	rowsIndexesIter = rowsIndexes.begin();
	std::advance(rowsIndexesIter, row);
	rowsIndexes.erase(rowsIndexesIter);

	
}