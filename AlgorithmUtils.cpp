#include "stdafx.h"

int Algorithms::Utils::computeWayLenght(std::vector<int> &way, TravelerInstance &travelerInstace) {
	int output = 0;
	for (int i = 0; i < way.size() - 1; i++) {
		output += travelerInstace.intCitiesTab[way[i]][way[i + 1]];
	}
	return output;
}

int Algorithms::Utils::factorial(unsigned int f) {
	unsigned int ret = 1;
	for (unsigned int i = 1; i <= f; ++i) {
		ret *= i;
	}
	return ret;
}

bool Algorithms::Utils::contains(std::vector<int>& vec, int from, int to, int value) {
	for (int i = from; i < to; i++) {
		if (vec[i] == value) {
			return true;
		}
	}
	return false;
}

std::vector<int> Algorithms::Utils::generateRandomSolution(int first, int size) {
	std::random_device rd;
	std::mt19937 engine(rd());
	std::vector<int> output = std::vector<int>(size + 1);
	output[0] = first;
	output[output.size() - 1] = first;
	int tempIndex = 1;
	for (int i = 1; i < size; i++) {
		if (i != first) {
			output[tempIndex] = i;
			tempIndex++;
		}
	}

	std::shuffle(output.begin() + 1, output.end() - 1, engine);
	return output;
}
