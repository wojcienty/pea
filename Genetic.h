#pragma once

#include "stdafx.h"

class Genetic {

private:
	std::random_device rd;
	std::mt19937 engine;
	std::uniform_int_distribution<int> mutationDistibution;
	std::uniform_int_distribution<int> swapDistibution;

	struct Comparator {
		Genetic& genetic;
		Comparator(Genetic& _genetic) : genetic(_genetic){}
		bool operator() (std::vector<int>& first, std::vector<int>& second);
	};

public:

	Genetic(TravelerInstance travelerInstance, int sizeOfPopulation = 1, int amountOfGenerations = 1);
	TravelerInstance travelerInstance;

	int sizeOfPopulation = 1;
	int amountOfGenerations = 1;

	std::vector<int> bestSolutionSoFar;
	std::vector<std::vector<int>> population;
	std::vector<std::vector<int>> selected;
	
	void selection();
	void mutation();
	void crossing();

};
