#pragma once

#include "stdafx.h"

class TabuSearch {
public:
	TravelerInstance instance;
	int tabuTenure = 30;
	typedef std::list<std::pair<int, int>> TabuList;
	TabuList tabuList;

	std::random_device rd;
	std::mt19937_64 engine;
	std::uniform_int_distribution<int> dist;

	int noBetterSolutionLoops = 0;
	int noBetterSolutionLimit = 100;


	TabuSearch(TravelerInstance instance);

	std::pair<int, int> makeRandomSwap(std::vector<int> &way); //Robi zamian� i zwraca zamienione indeksy
	std::vector<int> generateRandomSolution();

	bool isSwapInTabuList(std::pair<int, int> swap); // sprawdza czy swapa nie ma w li�cie tabu
	void updateTabuList();
	void addToTabuList(std::pair<int, int> swap);
	void diversification(std::vector<int> &bestWay, int * bestWayLenght);
};