#include "stdafx.h"


TravelerInstance::TravelerInstance() : startCity(-1) {

}

TravelerInstance::TravelerInstance(int randomBound, int size) : startCity(-1) {
	std::random_device rd;
	std::mt19937 engine(rd());
	std::uniform_int_distribution<int> distribution(0, randomBound);

	this->initializeIntCitiesTab(size);

	int random;
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			if (j == i) {
				this->intCitiesTab[i][j] = 0;
			} else {
				random = 0;
				while (random == 0) {
					random = distribution(engine);
				}
				this->intCitiesTab[i][j] = random;
			}
		}
	}
}

void TravelerInstance::initializeIntCitiesTab(int size) {
	this->intCitiesTab = std::vector<std::vector<int>>(size);
	for (int i = 0; i < size; i++) {
		intCitiesTab[i] = std::vector<int>(size);
	}
}

bool TravelerInstance::areNamesAvailable() {
	return citiesNameList != nullptr;
}

void TravelerInstance::show() {
	using namespace Utils::String;
	std::locale::global(std::locale("polish"));
	printSeparator();
	if (intCitiesTab.size() == 0) {
		printLine(L"Macie� s�siedztwa jest pusta!");
		printSeparator();
		return;
	} else {
		printLine(L"Rozmiar macie�y s�siedztwa = " + std::to_wstring(intCitiesTab.size()));
	}
	printSeparator();


	std::list<std::string>::iterator iter;
	std::string rowString;
	for (int i = 0; i < this->intCitiesTab.size(); i++) {
		rowString = "";
		if (this->areNamesAvailable()) {
			iter = citiesNameList->begin();
			std::advance(iter, i);
			rowString += "[" + *iter + "] : ";
		}
		for (int j = 0; j < intCitiesTab[i].size(); j++) {
			rowString += std::to_string(intCitiesTab[i][j]);
			if (j != intCitiesTab[i].size() - 1) {
				rowString += " : ";
			}
		}
		printLine(stringToWstring(rowString));
	}
	printSeparator();
}
