#pragma once

#include "stdafx.h"

class BnBMatrix {

public:
	static const u_char COLUMN;
	static const u_char ROW;
	static const char INF;

	std::vector<int> VU;
	std::vector<int> UeV;

	std::vector<int> rowsIndexes;
	std::vector<int> columsIndexes;

	std::vector<std::vector<int>> matrix;

	BnBMatrix(std::vector<std::vector<int>> matrix);

	int size();

	/*
	Wyznacza minimum z wybranego wiersza po czym odejmuje je od ka�dego elementu
	warning: podany index jest bezwzgl�dny
	return: wyznaczone minimum + index
	*/
	std::pair<int, u_int> getRowMinimum(int row);

	/*
	Wyznacza minimum z wybranej kolumny po czym odejmuje je od ka�dego elementu
	warning: podany index jest bezwzgl�dny
	return: wyznaczone minimum + index
	*/
	std::pair<int, u_int> getColumnMinimum(int column);


	/*
	Wyznacza minimum z wybranego wiersza uwzgl�dniaj�c licznik zer
	if (zer jest co najmniej 2) zwraca 0
	else zwraca znalezione minimum
	warning: podany index jest bezwzgl�dny
	return: wyznaczone minimum
	*/
	int getRowMinimumWithZeros(int row);

	/*
	Wyznacza minimum z wybranej kolumny uwzgl�dniaj�c licznik zer
	if (zer jest co najmniej 2) zwraca 0
	else zwraca znalezione minimum
	warning: podany index jest bezwzgl�dny
	return: wyznaczone minimum
	*/
	int getColMinimumWithZeros(int column);


	/*
	Zwraca index odnalezionego zera w kolumnie b�d� wierszu
	*/
	int findZeroIndex(u_char colOrRow, int index, std::vector<Edge> &edges);

	/*
	Wyznacza rz�d lub kolumn� do usuni�cia w zale�no�ci od znalezionego maximum minim�w(z zerami)
	zwraca par�: rz�d czy kolumna(sta�a klasy), index
	*/
	std::pair<u_char, int> getRowOrColToDelete(std::vector<int> &minimums);

	/*
	Usuwa dany wiersz i kolumn�
	*/
	void deleteRowAndCol(u_int row, u_int col, std::vector<Edge> &edges);

private:
	/*
	Sprawdza czy odnaleziony index zera jest ju� wyokrzystany //ochrona przed cyklami
	*/
	bool isInEdgesList(int value, std::vector<Edge> &edges);


	bool blockReturn(int row, int col);

	bool blockReturnOut(int row, int col);

};
