#include "stdafx.h"


DynamicSolver::DynamicSolver(TravelerInstance instance) {
	this->instance = instance;
}

std::vector<int> DynamicSolver::g(int from, std::vector<int> way) {
	if (way.size() == 2) {
		return way;
	}

	std::vector<int> tempVec;
	std::vector<int> tempMinVec;
	std::vector<int> minVec;
	int tempMin;
	int minIndex;
	int min = INT_MAX;

	for (int i = 0; i < way.size() - 1; i++) {
		std::vector<int> tempVec = way;
		tempVec.erase(tempVec.begin() + i);
		tempMinVec = g(way[i], tempVec);
		tempMin = instance.intCitiesTab[from][way[i]] + instance.intCitiesTab[way[i]][tempMinVec[0]] + Algorithms::Utils::computeWayLenght(tempMinVec, instance);
		if (tempMin < min) {
			min = tempMin;
			minIndex = i;
			minVec = tempMinVec;
		}
	}
	minVec.insert(minVec.begin(), way[minIndex]);
	return minVec;
}

void DynamicSolver::solve() {
	std::vector<std::vector<int>> cities = instance.intCitiesTab;
	std::vector<int> way = std::vector<int>(cities.size() + 1);
	way[0] = instance.startCity;
	way[way.size() - 1] = instance.startCity;

	int tempIndex = 1;
	for (int i = 1; i < cities.size(); i++) {
		if (i != instance.startCity) {
			way[tempIndex] = i;
			tempIndex++;
		}
	}
	int from = way[0];
	way.erase(way.begin());
	std::vector<int> output = g(from, way);
	output.insert(output.begin(), from);
	this->solution = output;
}

void DynamicSolver::numSolve() {
	std::vector<std::vector<int>> cities = instance.intCitiesTab;
	std::vector<int> way = std::vector<int>(cities.size() + 1);
	way[0] = instance.startCity;
	way[way.size() - 1] = instance.startCity;

	int tempIndex = 1;
	for (int i = 1; i < cities.size(); i++) {
		if (i != instance.startCity) {
			way[tempIndex] = i;
			tempIndex++;
		}
	}
	int from = way[0];
	way.erase(way.begin());
	this->numSolution = gNum(from, way);
}

int DynamicSolver::gNum(int from, std::vector<int> way) {
	if (way.size() == 2) {
		return instance.intCitiesTab[from][way[0]] + Algorithms::Utils::computeWayLenght(way, instance);
	}

	std::vector<int> tempVec;
	int tempMin;
	int min = INT_MAX;
	for (int i = 0; i < way.size() - 1; i++) {
		std::vector<int> tempVec = way;
		tempVec.erase(tempVec.begin() + i);
		tempMin = instance.intCitiesTab[from][way[i]] + gNum(way[i], tempVec);
		if (min > tempMin) {
			min = tempMin;
		}
	}
	return min;
}

void DynamicSolver::nonRecursionSolve() {}