#include "stdafx.h"

Result::Result(std::vector<int> way, clock_t time, std::wstring nameOfAlgorithm) : way(way), nameOfAlgorithm(nameOfAlgorithm) {
	this->time = (long double) time / (long double) CLOCKS_PER_SEC;
}
