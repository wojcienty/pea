#pragma once

#include "stdafx.h"

namespace Algorithms {
	namespace Utils {
		int computeWayLenght(std::vector<int> &way, TravelerInstance &travelerInstace);
		int factorial(unsigned int f);
		bool contains(std::vector<int> &vec, int from, int to, int value);
		std::vector<int> generateRandomSolution(int first, int size);
	}

	struct BnBMag {
		std::vector<Edge> edges;
		int upperBound;
		BnBMatrix matrix;
		BnBMag(BnBMatrix matrix, int upperBound, std::vector<Edge> edges) : matrix(matrix), upperBound(upperBound), edges(edges) {}
	};

	typedef std::list<BnBMag> CheckList;
	std::pair<std::vector<Edge>, int> branch(BnBMag mag, CheckList &toCheck, int bestUpper);

	Result branchAndBound(TravelerInstance instance);

	Result bruteForce(TravelerInstance instance);

	Result annealing(TravelerInstance instance);

	Result tabuSearch(TravelerInstance instance, int numberOfSteps);

	Result dynamic(TravelerInstance instance);

	Result genetic(TravelerInstance instance, int populationSize, int generationsAmount);

	Result approx(TravelerInstance instance);
}
