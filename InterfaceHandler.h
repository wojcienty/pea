#pragma once

#include "stdafx.h"

//Klasa menu wyboru algorytm�w itp
class InterfaceHandler {

public:

	TravelerInstance * instance = nullptr;
	bool loadedFromFile = false;
	InterfaceHandler();

	void showMenu();
	void makeTravelerInstance();
	void mainLoop();
	void clearConsole();
	void pressToContinue();
	void presentAlgorithmResult(Result result);
	void getStartCity();
	bool isInstanceReady();

	void computeAnnealing();
	void computeBranchAndBound();
	void computeTabuSearch();
	void computeBruteForce();
	void computeDynamic();
	void computeGenetic();
	void compute2approx();
};
