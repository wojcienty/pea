#pragma once
#include "stdafx.h"

class TravelerInstance {

public:
	TravelerInstance();


	//Konstruktor wype�niaj�cy list� miast losowymi liczbami
	TravelerInstance(int randomBound, int size);

	//opcjonalne dla polotu -> koljno�� ma znaczenie przy wy�wietlaniu
	std::list <std::string> * citiesNameList = nullptr;
	//int[][] gdzie : rz�dy = miasta, kolumny odleg�o�ci do poszczeg�lnych
	std::vector<std::vector<int>> intCitiesTab;

	int startCity = -1;

	bool areNamesAvailable();
	void initializeIntCitiesTab(int size);

	void show();
};
