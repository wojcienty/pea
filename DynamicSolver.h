#pragma once

#include "stdafx.h"

class DynamicSolver {
private:
	TravelerInstance instance;

public:
	DynamicSolver(TravelerInstance instance);
	std::vector<int> solution;
	int numSolution = INT_MAX;

	std::vector<int> g(int from, std::vector<int> way);
	int gNum(int from, std::vector<int> way);
	void solve();
	void nonRecursionSolve();
	void numSolve();

};