﻿#include "stdafx.h"

//1 APPROXIMATION - VERTEX - COVER(G) :
//2 C = ∅
//3 E'= G.E
//4
//5 while E'≠ ∅:
//6     let(u, v) be an arbitrary edge of E'
//7     C = C ∪{u, v}
//8     remove from E' every edge incident on either u or v
//9
//10 return C

void Approx2Solver::mst() {
	std::random_device rd;
	std::mt19937 engine(rd());
	
	std::uniform_int_distribution<int> distr(0, this->indexedMatrix.size() - 1);

	int row, col;
	int size = this->indexedMatrix.size();
	while (indexedMatrix.size() != 1) {
		do {
			row = distr(engine) % size;
			col = distr(engine) % size;
		} while (row == col || indexedMatrix.matrix[row][col] == -1);
		Edge edge = {this->indexedMatrix.availableRows[row], this->indexedMatrix.availableColumns[col]};
		edges.push_back(edge);

		this->indexedMatrix.deleteColumn(row);
		this->indexedMatrix.deleteRow(row);
		size--;
	}
}

void Approx2Solver::eulerCycle() {

	std::list<int> stack;
	std::set<int> cycle;

	int size = edges.size();

	std::vector<std::list<int>> adjencancyVec(this->instance.intCitiesTab.size());
	for (int i = 0; i < this->instance.intCitiesTab.size(); i++) {
		adjencancyVec[i] = std::list<int>();
	}
	for (int i = 0; i < size; i++) {
		//Podwajanie krawędzi
		adjencancyVec[edges[i].first].push_back(edges[i].second);
		adjencancyVec[edges[i].second].push_back(edges[i].first);
		edges.push_back({edges[i].second, edges[i].first});
	}

	int startCity = -1;
	for (int i = 0; i < adjencancyVec.size(); i++) {
		if (adjencancyVec[i].size() > 0) {
			startCity = i;
			stack.push_back(startCity);
			cycle.insert(startCity);
			break;
		}
	}

	while (cycle.size() != this->instance.intCitiesTab.size()) {
		int city = adjencancyVec[startCity].front();
		adjencancyVec[startCity].pop_front();
		stack.push_back(city);
		cycle.insert(city);
		if (adjencancyVec[city].size() == 0) {
			continue;
		}
		startCity = city;
	}

	this->eulerCycleList = stack;
}

void Approx2Solver::makeWay() {
	std::unordered_set<int> u_set;
	for (int i : this->eulerCycleList) {
		u_set.insert(i);
	}

	this->way = std::vector<int>(this->instance.intCitiesTab.size() + 1);
	this->way[0] = this->instance.startCity;
	this->way[this->instance.intCitiesTab.size()] = this->instance.startCity;

	std::unordered_set<int>::iterator iter = u_set.find(instance.startCity);

	for (int i = 1; i < way.size() - 1; i++) {
		std::advance(iter, 1);
		if (iter == u_set.end()) {
			iter = u_set.begin();
		} 
		if (*iter == way[0]) {
			break;
		}
		way[i] = *iter;
	}

}

Approx2Solver::Approx2Solver(TravelerInstance instance) {
	this->instance = instance;
}

Approx2Solver::Approx2Solver() {}

void Approx2Solver::prepareRepresentation() {
	this->indexedMatrix = IndexedMatrix(instance);
}

void Approx2Solver::IndexedMatrix::deleteRow(int index) {
	std::vector<std::vector<int>>::iterator iter = matrix.begin();
	std::advance(iter, index);
	matrix.erase(iter);

	std::vector<int>::iterator rowIter = availableRows.begin();
	std::advance(rowIter, index);
	availableRows.erase(rowIter);
}

void Approx2Solver::IndexedMatrix::deleteColumn(int index) {
	std::vector<int>::iterator colIter;
	for (int i = 0; i < matrix.size(); i++) {
		colIter = matrix[i].begin();
		std::advance(colIter, index);
		matrix[i].erase(colIter);
	}

	colIter = availableColumns.begin();
	std::advance(colIter, index);
	availableColumns.erase(colIter);
}

int Approx2Solver::IndexedMatrix::size() {
	return matrix.size();
}

Approx2Solver::IndexedMatrix::IndexedMatrix(TravelerInstance instance) {
	this->matrix = instance.intCitiesTab;
	this->availableRows = std::vector<int>(matrix.size());
	this->availableColumns = std::vector<int>(matrix.size());
	for (int i = 0; i < matrix.size(); i++) {
		availableColumns[i] = i;
		availableRows[i] = i;
	}
}

Approx2Solver::IndexedMatrix::IndexedMatrix() {}
