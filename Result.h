#pragma once

#include "stdafx.h"

class Result {

public:
	std::vector<int> way;
	long double time;
	std::wstring nameOfAlgorithm;
	Result(std::vector<int> way, clock_t time, std::wstring nameOfAlgorithm);

};