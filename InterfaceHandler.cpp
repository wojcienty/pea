#include "stdafx.h"


InterfaceHandler::InterfaceHandler() {
	std::locale::global(std::locale("polish"));
}

void InterfaceHandler::clearConsole() {
	system("cls");
}

void InterfaceHandler::pressToContinue() {
	Utils::String::printLine(L"Naci�nij dowolny klawisz by kontynuowa� [ESC by wyj��]");
	unsigned char keyCode = _getch();
	if (keyCode == 27) {
		exit(0);
	}
}

void InterfaceHandler::makeTravelerInstance() {
	std::wstring getIntMessage = L"Utw�rz instancj�: \n";
				 getIntMessage += L"\t 1. Z pliku \n";
				 getIntMessage += L"\t 2. Wygeneruj losowe warto�ci";

	std::wstring getStringMessage = L"Podaj scie�k� do pliku:";

	int choice = InputManager::getIntFromUser(getIntMessage, 2);

	if (this->instance != nullptr) {
		delete this->instance;
		this->instance = nullptr;
	}

	switch (choice) {
		case 1: {
			try {
				this->instance = new TravelerInstance();
				FileParser::parseInputFile(InputManager::getStringFromUser(getStringMessage), *this->instance);
				this->loadedFromFile = true;
			} catch (const wchar_t * error) {
				std::wcout << error << std::endl;
				delete this->instance;
				this->instance = nullptr;
			}
			break;
		}
		case 2: {
			int size = InputManager::getIntFromUser(L"Podaj rozmiar instancji:", -1);
			int bound = InputManager::getIntFromUser(L"Podaj g�rn� granic� losowej wagi:", -1);
			this->instance = new TravelerInstance(bound, size);
			break;
		}
		default:
			break;
	}

}

void InterfaceHandler::presentAlgorithmResult(Result result) {
	using namespace Utils::String;
	printLine(L"Wyniki dla: " + result.nameOfAlgorithm);

	int sumOfDistances = 0, actualDistance = 0;
	std::list<std::string>::iterator namesIterator;

	for (int i = 0; i < result.way.size() - 1; i++) {

		if (this->instance->areNamesAvailable()) {
			namesIterator = this->instance->citiesNameList->begin();
			std::advance(namesIterator, result.way[i]);
			std::cout << "Z " << *namesIterator;
		} else {
			std::cout << "Z " << std::to_string(result.way[i]);
		}

		if (this->instance->areNamesAvailable()) {
			namesIterator = this->instance->citiesNameList->begin();
			std::advance(namesIterator, result.way[i + 1]);
			std::cout << " do " << *namesIterator;
		} else {
			std::cout << " do " << std::to_string(result.way[i + 1]);
		}

		actualDistance = this->instance->intCitiesTab[result.way[i]][result.way[i + 1]];
		std::cout << " droga = " << std::to_string(actualDistance);
		sumOfDistances += actualDistance;

		printLine();
	}

	printLine(L"Sumaryczny dystans wyni�s� : " + std::to_wstring(sumOfDistances));
	printLine(L"Czas dzia�ania algorytmu wyni�s�: " + std::to_wstring(result.time) + L" s");
}

void InterfaceHandler::showMenu() {
	using namespace Utils::String;
	printSeparator();
	std::wcout << L"Projekt PEA -> Problem komiwoja�era" << std::endl;
	//Opis algorytmu jaki� doda� w miar� post�p�w
	printSeparator();
	if (this->instance == nullptr) {
		printLine(L"Brak instancji problemu!!!");
	} else {
		std::wstring state = L"";
		int size = this->instance->intCitiesTab.size();
		state += L"\t -Rozmiar instancji = " + std::to_wstring(size) + L"\n";
		if (loadedFromFile) {
			state += L"\t -Wygenerowano z pliku \n";
		} else {
			state += L"\t -Stworzono losowo \n";
		}

		if (this->instance->startCity == -1) {
			state += L"\t -Brak wybranego miasta startowego!";
		} else {
			state += L"\t -Miasto startowe -> nr " + std::to_wstring(this->instance->startCity);
			if (this->instance->areNamesAvailable()) {
				std::list<std::string>::iterator iter = this->instance->citiesNameList->begin();
				std:advance(iter, this->instance->startCity);
				state += L" -> [" + Utils::String::stringToWstring(*iter) + L"]";
			}
		}

		printLine(L"Stan instancji : ");
		printLine(state);
	}
	printSeparator();
	printLine(L"1. Utw�rz instancj� problemu komiwoja�era.");
	printLine(L"2. Wy�wietl zawarto�� instancji problemu.");
	printLine(L"3. Wybierz miasto startowe.");
	printLine(L"4. Wykonaj algorytm symulowanego wy�arzania.");
	printLine(L"5. Wykonaj algorytm podzia��w i ogranicze�.");
	printLine(L"6. Wykonaj algorytm lokalego poszukiwania TabuSearch.");
	printLine(L"7. Wykonaj przegl�d zupe�ny.");
	printLine(L"8. Wykonaj algorytm oparty o pragramowanie dynamiczne.");
	printLine(L"9. Wykonaj algorytm genetyczny.");
	printLine(L"10. Wykonaj algorytm 2-aproksymacyjny.");
	printSeparator();
}

void InterfaceHandler::mainLoop() {
	int choice;
	std::wstring message = L"Jaki jest Tw�j wyb�r?";
	int menuOptionsCount = 10;

	while (true) {
		this->showMenu();
		choice = InputManager::getIntFromUser(message, menuOptionsCount);

		switch (choice) {
			case 1: {
				this->makeTravelerInstance();
				break;
			}
			case 2: {
				if (this->instance != nullptr) {
					this->instance->show();
				} else {
					Utils::String::printLine(L"Nie utworzono jeszcze �adnej instancji!");
				}
				break;
			}
			case 3: {
				this->getStartCity();
				break;
			}
			case 4: {
				this->computeAnnealing();
				break;
			}
			case 5: {
				this->computeBranchAndBound();
				break;
			}
			case 6 : {
				this->computeTabuSearch();
				break;
			}
			case 7: {
				this->computeBruteForce();
				break;
			}
			case 8: {
				this->computeDynamic();
				break;
			}
			case 9: {
				this->computeGenetic();
				break;
			}
			case 10: {
				this->compute2approx();
				break;
			}
			default:
				break;
		}

		this->pressToContinue();
		this->clearConsole();
	}

}

void InterfaceHandler::getStartCity() {
	if (this->instance == nullptr) {
		Utils::String::printLine(L"Brak instancji problemu!");
		return;
	}

	std::wstring message = L"Wybierz miasto pocz�tkowe:";
	int city = InputManager::getIntFromUser(message, this->instance->intCitiesTab.size() - 1, 0);

	this->instance->startCity = city;

}

bool InterfaceHandler::isInstanceReady() {
	return this->instance != nullptr && this->instance->startCity != -1;
}

void InterfaceHandler::computeAnnealing() {

	if (!isInstanceReady()) {
		Utils::String::printLine(L"Nie przygotowano instancji!");
		return;
	}
	Result result = Algorithms::annealing(*instance);
	
	this->presentAlgorithmResult(result);
}

void InterfaceHandler::computeBranchAndBound() {
	if (!isInstanceReady()) {
		Utils::String::printLine(L"Nie przygotowano instancji!");
		return;
	}

	Result result = Algorithms::branchAndBound(*instance);
	this->presentAlgorithmResult(result);
}

void InterfaceHandler::computeTabuSearch() {
	if (!isInstanceReady()) {
		Utils::String::printLine(L"Nie przygotowano instancji!");
		return;
	}

	using namespace Utils::String;
	this->clearConsole();
	printSeparator();
	printLine(L"Kreator algorytmu przeszukiwania lokalnego TabuSearch.");
	printSeparator();

	int numberOfSteps = InputManager::getDoubleFromUser(L"Podaj ilo�� krok�w algorytmu:", -1);

	Result result = Algorithms::tabuSearch(*instance, numberOfSteps);
	this->presentAlgorithmResult(result);
}

void InterfaceHandler::computeBruteForce() {
	if (!isInstanceReady()) {
		Utils::String::printLine(L"Nie przygotowano instancji!");
		return;
	}

	Result result = Algorithms::bruteForce(*instance);
	this->presentAlgorithmResult(result);
}

void InterfaceHandler::computeDynamic() {
	if (!isInstanceReady()) {
		Utils::String::printLine(L"Nie przygotowano instancji!");
		return;
	}

	Result result = Algorithms::dynamic(*instance);
	this->presentAlgorithmResult(result);
}

void InterfaceHandler::computeGenetic() {
	if (!isInstanceReady()) {
		Utils::String::printLine(L"Nie przygotowano instancji!");
		return;
	}

	using namespace Utils::String;
	this->clearConsole();
	printSeparator();
	printLine(L"Kreator algorytmu genetycznego.");
	printSeparator();

	int generations = InputManager::getDoubleFromUser(L"Podaj ilo�� pokole�:", -1);
	int sizeOfPopulation = InputManager::getDoubleFromUser(L"Podaj wielko�� populacji:", -1);

	Result result = Algorithms::genetic(*instance, sizeOfPopulation, generations);
	this->presentAlgorithmResult(result);
}

void InterfaceHandler::compute2approx() {
	if (!isInstanceReady()) {
		Utils::String::printLine(L"Nie przygotowano instancji!");
		return;
	}

	Result result = Algorithms::approx(*instance);
	this->presentAlgorithmResult(result);
}
