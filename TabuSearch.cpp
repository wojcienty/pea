#include "stdafx.h"


TabuSearch::TabuSearch(TravelerInstance instance) {
	this->instance = instance;
	this->engine = std::mt19937_64(rd());
	this->dist = std::uniform_int_distribution<int>(1, this->instance.intCitiesTab.size() - 2);
}

std::vector<int> TabuSearch::generateRandomSolution() {
	std::vector<int> output = std::vector<int>(instance.intCitiesTab.size() + 1);
	output[0] = instance.startCity;
	output[output.size() - 1] = instance.startCity;
	int tempIndex = 1;
	for (int i = 1; i < instance.intCitiesTab.size(); i++) {
		if (i != instance.startCity) {
			output[tempIndex] = i;
			tempIndex++;
		}
	}

	std::shuffle(output.begin() + 1, output.end() - 1, engine);
	return output;
}

std::pair<int, int> TabuSearch::makeRandomSwap(std::vector<int> &way) {
	int firstIndex;
	int secondIndex;
	do {
		firstIndex = dist(engine);
		secondIndex = dist(engine);
	} while (firstIndex == secondIndex);
	::Utils::Vector::swap(way, firstIndex, secondIndex);
	return std::pair<int, int>(firstIndex, secondIndex);
}

bool TabuSearch::isSwapInTabuList(std::pair<int, int> swap) {
	for (std::list<std::pair<int, int>>::iterator iter = tabuList.begin(); iter != tabuList.end(); iter++) {
		auto actual = *iter;
		if (actual.first == swap.first && actual.second == swap.second) {
			return true;
		}
	}
	return false;
}

void TabuSearch::updateTabuList() {
	if (tabuList.size() == tabuTenure) {
		tabuList.pop_front();
	}
}

void TabuSearch::addToTabuList(std::pair<int, int> swap) {
	tabuList.insert(tabuList.end(), swap);
}

void TabuSearch::diversification(std::vector<int> &bestWay, int * bestWayLenght) {
	if (noBetterSolutionLoops > noBetterSolutionLimit) {
		///?????
		/*In order to avoid that a large region of the state space graph remains completely unexplored it
		 is important to diversify the search. The simplest way to do it is to perform several random
		 restarts*/
		std::vector<int> way;
		std::vector<int> best;
		int actualSolutionLenght = -1;
		int bestSolutionLenght = INT_MAX;
		for (int i = 0; i < 10; i++) {
			way = generateRandomSolution();
			actualSolutionLenght = Algorithms::Utils::computeWayLenght(way, instance);
			if (actualSolutionLenght < bestSolutionLenght) {
				bestSolutionLenght = actualSolutionLenght;
				best = way;
			}
		}		

		*bestWayLenght = bestSolutionLenght;
		for (int i = 0; i < bestWay.size(); i++) {
			bestWay[i] = best[i];
		}
		noBetterSolutionLoops = 0;
	} 
}



