#pragma once

#include "stdafx.h"

namespace InputManager {
	int getIntFromUser(std::wstring message, int bound, int start = 1);
	double getDoubleFromUser(std::wstring message, double bound, double start = 0);
	std::string getStringFromUser(std::wstring message);
}

