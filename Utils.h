#pragma once
#include "stdafx.h"

namespace Utils {
	namespace String {
		bool isLineComment(std::string &string);
		void split(std::string &string, std::string separator, std::vector<std::string> &outputVector);
		bool isEmpty(std::string &string);
		bool startsWith(std::string &string, std::string start);
		std::wstring stringToWstring(std::string &string);
		extern std::wstring LINE_SEPARATOR;

		void printLine(std::wstring wstring = L"");
		void print(std::wstring wstring = L"");
		void printSeparator();
	}

	namespace Vector {
		void swap(std::vector<int> &vec, unsigned int index1, unsigned int index2);
	}
}
