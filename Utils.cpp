
#include "stdafx.h"

std::wstring Utils::String::LINE_SEPARATOR = L"----------------------------------------";

bool Utils::String::isLineComment(std::string &string) {
	return boost::starts_with(string, "//");
}

void Utils::String::split(std::string &string, std::string separator, std::vector<std::string> &output) {
	boost::split(output, string, boost::is_any_of(separator));
}

bool Utils::String::isEmpty(std::string &string) {
	boost::trim(string);
	return string.size() == 0;
}

bool Utils::String::startsWith(std::string &string, std::string start) {
	return boost::starts_with(string, start);
}

std::wstring Utils::String::stringToWstring(std::string &string) {
	std::wstring output;
	output.assign(string.begin(), string.end());
	return output;
}

void Utils::Vector::swap(std::vector<int> &vec, unsigned int index1, unsigned int index2) {
	int swapBuffer;
	swapBuffer = vec[index2];
	vec[index2] = vec[index1];
	vec[index1] = swapBuffer;
}

void Utils::String::printSeparator() {
	Utils::String::printLine(Utils::String::LINE_SEPARATOR);
}


void Utils::String::printLine(std::wstring wstring) {
	std::wcout << wstring << std::endl;
}

void Utils::String::print(std::wstring wstring) {
	std::wcout << wstring;
}