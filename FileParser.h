#pragma once

#include "stdafx.h"

class FileParser {
public:
	static const int PARAMS_AMOUNT;
	static const std::string TRUE_STRING;
	static const std::string SIZE_PARAM;
	static const std::string NAMES_IN_PARAM;
	static const std::string SPLIT_SEPARATOR;
	static void parseInputFile(std::string filePath, TravelerInstance & instance);
};