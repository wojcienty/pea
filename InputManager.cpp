#include "stdafx.h"

//Rozpi�d�a si� je�eli si� poda doubla, nie mam pojecia jak wyczy�ci� stdina // fflush ani cin.clear() nie dzia�aj�
int InputManager::getIntFromUser(std::wstring message, int bound, int start) {
	using namespace Utils::String;

	int output;
	printLine(message);
	bool validInput = false;
	while (!validInput) {
		if (bound == -1) {
			printLine(L"Podaj dowoln� liczb� wi�ksz� od 0");
		} else {
			printLine(L"Podaj liczb� z zakresu od " + std::to_wstring(start) + L" do " + std::to_wstring(bound));
		}

		std::cin >> output;

		if (std::cin.good() && (bound != -1 ? output <= bound : true) && (bound == -1 ? output > 0 : output >= start)) {
			validInput = true;
		} else {
			printLine(L"Nie wykonano prawid�owo polecenia!");
		}
		std::cin.clear();
	}
	return output;
}

double InputManager::getDoubleFromUser(std::wstring message, double bound, double start /* = 0 */) {
	using namespace Utils::String;

	double output;
	std::wcout << message << std::endl;
	bool validInput = false;
	while (!validInput) {
		if (bound == -1) {
			printLine(L"Podaj dowoln� liczb� wi�ksz� od 0");
		} else {
			printLine(L"Podaj liczb� z zakresu od " + std::to_wstring(start) + L" do " + std::to_wstring(bound));
		}

		std::cin >> output;

		if (std::cin.good() && (bound != -1 ? output <= bound : true) && (bound == -1 ? output > 0 : output >= start)) {
			validInput = true;
		} else {
			printLine(L"Nie wykonano prawid�owo polecenia!");
		}
		std::cin.clear();
	}
	return output;
}

std::string InputManager::getStringFromUser(std::wstring message) {
	using namespace Utils::String;

	printLine(message);
	std::string output;
	bool validInput = false;
	while (!validInput) {
		std::cin >> output;
		if (std::cin.good()) {
			validInput = true;
		} else {
			printLine(L"Nie wykonano prawid�owo polecenia!");
		}
		std::cin.clear();
	}
	return output;
}