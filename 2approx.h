#pragma once
#include "stdafx.h"

class Approx2Solver {
public:
	std::list<int> eulerCycleList;
	std::vector<int> way;
	TravelerInstance instance;
	std::vector<Edge> edges;

	Approx2Solver();
	Approx2Solver(TravelerInstance instance);
	void prepareRepresentation();
	void mst();
	void eulerCycle();
	void makeWay();

	struct IndexedMatrix {
		std::vector<std::vector<int>> matrix;
		std::vector<int> availableRows;
		std::vector<int> availableColumns;

		void deleteRow(int index);
		void deleteColumn(int index);
		int size();

		IndexedMatrix(TravelerInstance instance);
		IndexedMatrix();
	};

	IndexedMatrix indexedMatrix;

};
